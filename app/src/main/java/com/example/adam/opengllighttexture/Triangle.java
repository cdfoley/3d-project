package com.example.adam.opengllighttexture;

import android.content.Context;
import android.opengl.GLES20;
import android.util.Log;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;

/**
 * Created by Adam on 22/11/2014.
 */
public class Triangle {

    private final int bytesPerFloat = 4;
    private final int membersPerVertex = 10;
    private final int strideBytes = membersPerVertex*bytesPerFloat;
    private final int positionOffset = 0;   //where in the buffer position stuff is
    private final int positionDataSize = 3; //how much position stuff there is
    private final int colorOffset = 3;
    private final int colorDataSize = 4;
    private final int normalOffset = 7;
    private final int normalDataSize = 3;
    private final FloatBuffer triangleBuffer;
    private int programHandle;

    //we need handles to data so we can pass it to the shader program
    private int MVPMatrixHandle;
    private int positionHandle;
    private int colorHandle;
    private int normalHandle;
    private int lightPosHandle;
    private int MVMatrixHandle;


    final float[] triangle1VerticesData={
            //x,y,z
            //r,g,b,a
            //x, y, z -- normal
            -0.5f, -0.25f, 0.0f,    //bottom left
            1.0f, 1.0f, 1.0f, 0.0f,
            0.0f, 0.0f, 1.0f,

            0.5f, -0.25f, 0.0f,    //bottom right
            1.0f, 1.0f, 1.0f, 0.0f,
            0.0f, 1.0f, 0.0f,

            0.0f, 0.56f, 0.0f,    //top
            1.0f, 1.0f, 1.0f, 0.0f,
            1.0f, 0.0f, 0.0f
    };

    public Triangle(Context mActivityContext){

        //initialise the buffer
        triangleBuffer = ByteBuffer.allocateDirect(triangle1VerticesData.length * bytesPerFloat)
                .order(ByteOrder.nativeOrder()).asFloatBuffer();

        triangleBuffer.put(triangle1VerticesData).position(0);

        // read in shader source code
        final String vertexShader = RawResourceReader.readTextFileFromRawResource(mActivityContext, R.raw.vertexshadersource);
        final String fragmentShader = RawResourceReader.readTextFileFromRawResource(mActivityContext, R.raw.fragmentshadersource);

        final int initVertexShaderHandle = GLES20.glCreateShader(GLES20.GL_VERTEX_SHADER);
        final int initFragmentShaderHandle = GLES20.glCreateShader(GLES20.GL_FRAGMENT_SHADER);
        // compile shaders, keep a handle to them
        final int vertexShaderHandle = ShaderHelper.compileShader(GLES20.GL_VERTEX_SHADER, vertexShader, initVertexShaderHandle);
        final int fragmentShaderHandle = ShaderHelper.compileShader(GLES20.GL_FRAGMENT_SHADER, fragmentShader, initFragmentShaderHandle);

        programHandle = GLES20.glCreateProgram();
        String[] attributes = {"a_Position", "a_Color", "a_Normal", "u_MVMatrix", "u_MVPMatrix", "u_LightPos"};
        //create a program object, store a handle to it
        if (programHandle != 0)
        {
// Bind the vertex shader to the program.
            GLES20.glAttachShader(programHandle, vertexShaderHandle);
// Bind the fragment shader to the program.
            GLES20.glAttachShader(programHandle, fragmentShaderHandle);
// Bind attributes
            if (attributes != null)
            {
                final int size = attributes.length;
                for (int i = 0; i < size; i++)
                {
                    GLES20.glBindAttribLocation(programHandle, i, attributes[i]);
                }
            }
// Link the two shaders together into a program.
            GLES20.glLinkProgram(programHandle);
// Get the link status.
            final int[] linkStatus = new int[1];
            GLES20.glGetProgramiv(programHandle, GLES20.GL_LINK_STATUS, linkStatus, 0);
// If the link failed, delete the program.
            if (linkStatus[0] == 0)
            {
                Log.e("Linker Error", "Error compiling program: " + GLES20.glGetProgramInfoLog(programHandle));
                GLES20.glDeleteProgram(programHandle);

                programHandle = 0;
            }
        }
        if (programHandle == 0)
        {
            throw new RuntimeException("Error creating program.");
        }

        MVPMatrixHandle = GLES20.glGetUniformLocation(programHandle, "u_MVPMatrix");
        positionHandle = GLES20.glGetAttribLocation(programHandle, "a_Position");
        colorHandle = GLES20.glGetAttribLocation(programHandle, "a_Color");
        normalHandle = GLES20.glGetAttribLocation(programHandle, "a_Normal");
        lightPosHandle = GLES20.glGetAttribLocation(programHandle, "u_LightPos");
        MVMatrixHandle = GLES20.glGetAttribLocation(programHandle, "u_MVMatrix");
    }

    public void draw(float[] mvMatrix, float[] mvpMatrix, float[] mLightPosInEyeSpace){
        GLES20.glUseProgram(programHandle);

        triangleBuffer.position(positionOffset);//read from the buffer where position is stored
        GLES20.glVertexAttribPointer(positionHandle, positionDataSize, GLES20.GL_FLOAT, false,
                strideBytes, triangleBuffer);
        GLES20.glEnableVertexAttribArray(positionHandle);

        //pass in colour info
        triangleBuffer.position(colorOffset);
        GLES20.glVertexAttribPointer(colorHandle, colorDataSize, GLES20.GL_FLOAT, false,
                strideBytes, triangleBuffer);
        GLES20.glEnableVertexAttribArray(colorHandle);

        //pass in normal data
        triangleBuffer.position(normalOffset);
        GLES20.glVertexAttribPointer(normalHandle, normalDataSize, GLES20.GL_FLOAT, false,
                strideBytes, triangleBuffer);
        GLES20.glEnableVertexAttribArray(normalHandle);

        //pass in MV Matrix
        GLES20.glUniformMatrix4fv(MVMatrixHandle, 1, false, mvMatrix, 0);

        GLES20.glUniform3f(lightPosHandle, mLightPosInEyeSpace[0], mLightPosInEyeSpace[1], mLightPosInEyeSpace[2]);

        GLES20.glUniformMatrix4fv(MVPMatrixHandle, 1, false, mvpMatrix, 0);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLES, 0, 3);

    }


}
