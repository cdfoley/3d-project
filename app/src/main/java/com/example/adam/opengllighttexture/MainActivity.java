package com.example.adam.opengllighttexture;

import android.app.Activity;
import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.Matrix;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import java.nio.FloatBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;


public class MainActivity extends Activity {

    private GLSurfaceView myGLSurfaceView;
    public Context activityContext = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //create a new GLSurfaceView
        myGLSurfaceView = new GLSurfaceView(this);
        //set OpenGL version = 2
        myGLSurfaceView.setEGLContextClientVersion(2);
        myGLSurfaceView.setEGLConfigChooser(8,8,8,8,16,0);
        //set renderer to be basicRenderer
        myGLSurfaceView.setRenderer(new basicRenderer(this));
        //show the OpenGL surface in the activity
        setContentView(myGLSurfaceView);


    }

    @Override
    protected void onResume(){
        super.onResume();
        myGLSurfaceView.onResume();
    }

    @Override
    protected void onPause(){
        super.onPause();
        myGLSurfaceView.onPause();
    }


}